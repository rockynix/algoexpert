# Very Hard - 0/32

* [ ] Max Profit With K Transactions
* [ ] Apartment Hunting
* [ ] Calendar Matching
* [ ] Waterfall Streams
* [ ] Minimum Area Rectangle
* [ ] Line Through Points
* [ ] Right Smaller Than
* [ ] Iterative In-order Traversal
* [ ] Flatten Binary Tree
* [ ] Right Sibling Tree
* [ ] All Kinds Of Node Depths
* [ ] Palindrome Partitioning Min Cuts
* [ ] Longest Increasing Subsequence
* [ ] Longest String Chain
* [ ] Square of Zeroes
* [ ] Knuth—Morris—Pratt Algorithm
* [ ] A* Algorithm
* [ ] Rectangle Mania
* [ ] Detect Arbitrage
* [ ] Airport Connections
* [ ] Merge Sorted Arrays
* [ ] LRU Cache
* [ ] Rearrange Linked List
* [ ] Linked List Palindrome
* [ ] Zip Linked List
* [ ] Node Swap
* [ ] Number Of Binary Tree Topologies
* [ ] Non-Attacking Queens
* [ ] Merge Sort
* [ ] Count Inversions
* [ ] Smallest Substring Containing
* [ ] Longest Balanced Substring